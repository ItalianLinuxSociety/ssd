<?php

function handlePage($link, $folder)
{
	if (strncmp($link, 'http', 4) != 0) {
		$link = 'https://ssd.eff.org' . $link;
	}

	$pagename = basename($link);
	$filepath = sprintf('en/%s/%s', $folder, $pagename);
	if (file_exists($filepath)) {
		return;
	}

	$page = file_get_contents($link);

	$pagedoc = new DOMDocument();
	$pagedoc->loadHTML($page, LIBXML_NOERROR);
	$xpath = new DOMXpath($pagedoc);

	$clean_style = $xpath->query("//*[@style]");
	foreach($clean_style as $cs) {
		$cs->removeAttribute('style');
	}

	/*
		Il glossario interattivo sara' poi sistemato a
		posteriori, date le traduzioni
	*/
	$clean_glossary = $xpath->query("//*[contains(@class, 'glossify-link')]");
	foreach($clean_glossary as $cg) {
		$cg->removeAttribute('title');
		$cg->removeAttribute('data-title');
		$cg->removeAttribute('data-placement');
		$cg->removeAttribute('data-trigger');
		$cg->removeAttribute('data-html');
		$cg->removeAttribute('data-toggle');
		$cg->removeAttribute('data-container');
		$cg->removeAttribute('data-content');
	}

	$content = $xpath->query("//section[contains(@class, 'main-section')]");
	$page = $content->item(0);
	$page = $page->ownerDocument->saveHTML($page);

	file_put_contents($filepath, (string) $page);

	echo "$link >>> $pagename\n";
}

function handleSet($homepage, $folder)
{
	$home = file_get_contents($homepage);

	$doc = new DOMDocument();
	$doc->loadHTML($home, LIBXML_NOERROR);
	$xpath = new DOMXpath($doc);
	$elements = $xpath->query("//a[contains(@href, '/" . $folder . "/')]");

	foreach($elements as $el) {
		$link = $el->getAttribute('href');
		handlePage($link, $folder);
	}
}

handleSet('https://ssd.eff.org/', 'module');
handleSet('https://ssd.eff.org/en/glossary', 'glossary');

echo "\n";

