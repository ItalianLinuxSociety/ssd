<section class="main-section">
        <a id="main-content"></a>

        
                <div class="breadcrumb">
          <a href="/en/module-categories/further-learning">Further Learning</a>        </div>
        
            <h1 class="page-header">Key Concepts in Encryption</h1>
        
                                          
      
    <div id="node-224" class="node node-article view-mode-full clearfix">

  
        
  
  <div class="content">
    <div class="container">
      
          </div>
    <div class="field field-name-field-updated field-type-datestamp field-label-inline clearfix field-wrapper"><div class="field-label">Last reviewed: </div><span class="date-display-single">11-26-2018</span></div><div class="body field"><p>Under some circumstances, <a href="/en/glossary/encryption" class="glossify-link">encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> can be fairly automatic and simple. But there are ways encryption can go wrong. The more you understand it, the safer you will be against such situations. We recommend reading the “<a href="https://ssd.eff.org/en/node/36">What Should I Know About Encryption?</a>” guide first if you haven’t already.</p>
<p>In this guide, we will look at five main ideas. These are important concepts for understanding encryption in transit:</p>
<ul><li>A cipher, a <a href="/en/glossary/key" class="glossify-link">key<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a></li>
<li>Symmetric and asymmetric encryption</li>
<li>Private and public keys</li>
<li>Identity verification for people (public key fingerprints)</li>
<li>Identity verification for websites (security certificates)</li>
</ul><a class="anchor-link-target" name="0">   </a> <h2>A Cipher, A Key <a class="anchor-link-icon" href="#0">Anchor link</a></h2>
<p>You’ve probably seen something that, on its face, is not understandable to you. Maybe it looks like it’s in another language, or like it’s gibberish—there’s some sort of barrier to being able to read and understand it. This doesn’t necessarily mean that it’s encrypted.</p>
<p>What differentiates something that is not understandable from something that’s <em>encrypted</em>?</p>
<p><strong>Encryption</strong> is a mathematical process used to scramble information, so that it can be unscrambled only with special knowledge. The process involves a cipher and a key.</p>
<p>A <strong>cipher</strong> is a set of rules (an algorithm) for encrypting and decrypting. These are well-defined steps that can be followed as a formula.</p>
<p>A <strong>key </strong>is a piece of information that instructs the cipher in how to <a href="/en/glossary/encrypt" class="glossify-link">encrypt<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> and <a href="/en/glossary/decrypt" class="glossify-link">decrypt<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>. Keys are one of the most important concepts for understanding encryption.</p>
<a class="anchor-link-target" name="0">   </a> <h2>One Key or Many Keys? <a class="anchor-link-icon" href="#0">Anchor link</a></h2>
<p>In <strong>symmetric encryption,</strong> there is one single key to both encrypt and decrypt information.</p>
<table cellspacing="1" cellpadding="1" border="2" align="center"><tbody><tr><td><img alt="" src="/files/2018/11/20/1.symmetric-key-simple.png" width="1200" height="600"></td>
</tr><tr><td class="rtecenter"><em>Older forms of encryption were symmetric. For the “Caesar cipher” used by Julius Caesar, the key to encrypt and decrypt a message was a shift of three. For example, “A” would be changed to “D.” The message “ENCRYPTION IS COOL” would be encrypted to “HQFUBSWLRQ LV FRRO” using the key of three. That same key would be used to decrypt it back to the original message.</em></td>
</tr></tbody></table><p>Symmetric encryption is still used today—it often comes in the form of “stream ciphers” and “block ciphers,” which rely on complex mathematical processes to make their encryption hard to crack. Encryption today includes many steps of scrambling <a href="/en/glossary/data" class="glossify-link">data<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> to make it hard to reveal the original content without the valid key. Modern symmetric encryption algorithms, such as the Advanced Encryption Standard (AES) algorithm, are strong and fast. Symmetric encryption is widely used by computers for tasks like encrypting files, encrypting partitions on a computer, completely encrypting devices and computers using full-disk encryption, and encrypting databases like those of <a href="https://ssd.eff.org/en/node/52">password managers</a>. To decrypt this symmetrically-encrypted information, you’ll often be prompted for a <a href="/en/glossary/password" class="glossify-link">password<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>. This is why we recommend using strong passwords, and <a href="https://ssd.eff.org/en/node/23">provide tutorials for generating strong passwords to protect this encrypted information.</a></p>
<p>Having a single key can be great if you are the only person who needs to access that information. But there’s a problem with having a single key: what if you wanted to share encrypted information with a friend far away? What if you couldn’t meet with your friend in person to share the private key? How could you share the key with your friend over an open Internet connection?</p>
<p><strong>Asymmetric encryption</strong>, also known as <strong><a href="/en/glossary/public-key-encryption" class="glossify-link">public key encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a></strong>, addresses these problems. Asymmetric encryption involves two keys: a private key (for decryption) <em>and</em> a public key (for encryption).</p>
<p><img alt="" src="/files/2018/11/20/2.symmetric-asymmetric.png" width="898" height="449"></p>
<table cellspacing="1" cellpadding="1" border="1" align="center"><thead><tr><th scope="col">
<p><strong>Symmetric Encryption</strong></p>
</th>
<th scope="col">
<p><strong>Asymmetric Encryption</strong></p>
</th>
</tr></thead><tbody><tr><td>
<ul><li>Fast</li>
</ul></td>
<td>
<ul><li>Slow</li>
</ul></td>
</tr><tr><td>
<ul><li>Doesn’t require a lot of computing power</li>
</ul></td>
<td>
<ul><li>Requires a lot of computing power</li>
</ul></td>
</tr><tr><td>
<ul><li>Useful for encrypting both large and small messages</li>
</ul></td>
<td>
<ul><li>Useful for encrypting small messages</li>
</ul></td>
</tr><tr><td>
<ul><li>Requires sharing the key for encryption and decryption</li>
</ul></td>
<td>
<ul><li>The decryption key does not need to be shared — only the “public key” for encryption is shared</li>
</ul></td>
</tr><tr><td>
<ul><li>Cannot be used for verifying identities (authentication)</li>
</ul></td>
<td>
<ul><li>Can be used for identity verification (authentication)</li>
</ul></td>
</tr></tbody></table><p>Symmetric and asymmetric encryption are often used together for encrypting data in transit.</p>
<a class="anchor-link-target" name="1">   </a> <h2>Asymmetric <a href="/en/glossary/encryption" class="glossify-link">Encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>: Private and Public Keys <a class="anchor-link-icon" href="#1">Anchor link</a></h2>
<p>Private and public keys come in matched pairs, because the private <a href="/en/glossary/key" class="glossify-link">key<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> and public key are mathematically tied together. You can think of it like a rock that is split in half. When held back together, the two halves fit in place to form the whole. No other rock-half will do. The public key and private key files are much the same, but are ultimately composed of computer-readable representations of <em>very</em> large numbers.</p>
<p><img alt="" src="/files/2018/11/20/3.file-keypair.png" width="761" height="761"></p>
<p class="tip">Although it is called a “public key,” it can be confusing to think of the public key as an actual, literal key to open things. It doesn’t quite serve that function. For more in-depth information on public keys and private keys, see SSD’s<a href="https://ssd.eff.org/en/node/50"> deep dive on public key cryptography</a>.</p>
<table cellspacing="1" cellpadding="1" border="1" align="center"><tbody><tr><td><img alt="" src="/files/2018/11/20/4.pubkeyinit.png" width="736" height="490"></td>
</tr><tr><td>
<p>A public key is a file that you can give to anyone or publish publicly. When someone wants to send you an end-to-end encrypted message, they’ll need your public key to do so.</p>
</td>
</tr></tbody></table><table cellspacing="1" cellpadding="1" border="1" align="center"><tbody><tr><td>
<p><img alt="" src="/files/2018/11/20/5.privkeyinit.png" width="988" height="658"></p>
</td>
</tr><tr><td>
<p>Your private key lets you <a href="/en/glossary/decrypt" class="glossify-link">decrypt<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> this encrypted message. Because your private key allows you to read encrypted messages, it becomes very important to protect your private key. In addition, your private key can be used to sign documents so that others can verify that they really came from you.</p>
</td>
</tr></tbody></table><p>Since the private key is ultimately a file on a device that requires protection, we encourage you to <a href="/en/glossary/password" class="glossify-link">password<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> protect and <a href="/en/glossary/encrypt" class="glossify-link">encrypt<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> the device where the private key is stored. On Surveillance Self-Defense, we have guides for <a href="https://ssd.eff.org/en/module/creating-strong-passwords">strong passwords</a> and <a href="https://ssd.eff.org/en/module/how-encrypt-your-iphone">device encryption</a>.</p>
<p><img alt="" src="/files/2018/11/20/6.file-keypair.png" width="876" height="876"></p>
<table cellspacing="1" cellpadding="1" border="1" align="center"><thead><tr><th scope="col">
<p><strong>Public Key</strong></p>
</th>
<th scope="col">
<p><strong>Private Key</strong></p>
</th>
</tr></thead><tbody><tr><td>
<ul><li>A file that can be shared widely (can be shared over the Internet easily)</li>
</ul></td>
<td>
<ul><li>A file that must be kept safe and protected</li>
</ul></td>
</tr><tr><td>
<ul><li>Sender needs the public key to encrypt information to the recipient</li>
</ul></td>
<td>
<ul><li>Used to decrypt encrypted messages that are addressed to the matched public key</li>
</ul></td>
</tr><tr><td>
<ul><li>Represented by a “public <a href="/en/glossary/key-fingerprint" class="glossify-link">key fingerprint<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>,” which is used for verifying identities (authentication)</li>
</ul></td>
<td>
<ul><li>Used for digital signatures, allowing a way to verify a sender’s identity (authentication)</li>
</ul></td>
</tr><tr><td>
<ul><li>Can be optionally posted to permanent, publicly-accessible databases, such as “keyservers” (keyservers are prominent in <a href="/en/glossary/pgp" class="glossify-link">PGP<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> encrypted email)</li>
</ul></td>
<td> </td>
</tr></tbody></table><p>In some ways, you can think of sending information in transit like sending a postcard. In the postcard illustration on the left (below), a sender writes: “HI! :-)” The sender addresses it to the message recipient. This message is unencrypted, and anyone passing the message along the way can read it.</p>
<p><img alt="" src="/files/2018/11/20/7.comparison-encryptedmessage.png" width="1485" height="495"></p>
<p>On the right is that same postcard, with the message encrypted between the sender and receiver. The message still conveys the message “Hi! :-)” but now it looks like a block of encrypted gibberish to the rest of us.</p>
<p>How is this done? The sender has found the recipient’s public key. The sender addresses the message to the recipient’s public key, which encrypts the message. The sender has also included their signature to show that the encrypted message is really from them.</p>
<p>Note that the <a href="https://ssd.eff.org/en/module/why-metadata-matters">metadata</a>—of who is sending and who is receiving the message, as well as additional information like time sent and received, where it passed through, and so on—is still visible. We can see that the sender and receiver are using encryption, we can tell that they are communicating, but we can’t read the content of their message.</p>
<a class="anchor-link-target" name="2">   </a> <h2>Who Are You Encrypting To? Are They Who They Really Say They Are? <a class="anchor-link-icon" href="#2">Anchor link</a></h2>
<p>Now, you might be wondering: “I get that my public <a href="/en/glossary/key" class="glossify-link">key<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> lets someone send me an encrypted message, and that my private key lets me read that encrypted message. But what if someone pretends to be me? What if they create a new public and private key, and impersonate me?”</p>
<p>That’s where <a href="/en/glossary/public-key-encryption" class="glossify-link">public key cryptography<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> is especially useful: It lets you verify your identity and your recipient’s identity. Let’s look at the capabilities of the private key more closely.</p>
<p><img alt="" src="/files/2018/11/20/8.privatekey.png" width="787" height="787"></p>
<p>In addition to letting you read encrypted messages that are sent to your public key, your private key lets you place unforgeable digital signatures on messages you send to other people, as though to say “yes, this is really me writing this.”</p>
<p>Your recipient will see your <a href="/en/glossary/digital-signature" class="glossify-link">digital signature<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> along with your message and compare it with the information listed from your public key.</p>
<p>Let’s look at how this works in practice.</p>
<a class="anchor-link-target" name="2">   </a> <h2>Identity Verification for People: Public Key Fingerprints <a class="anchor-link-icon" href="#2">Anchor link</a></h2>
<p>When we send any kind of message, we rely on the good faith of people participating. It’s like in the real world: We don’t expect a mail delivery person to meddle with the contents of our mail, for example. We don’t expect someone to intercept a friend’s letter to us, open and modify it, and send it to us, as though nothing had been changed. But there’s a <a href="/en/glossary/risk-assessment" class="glossify-link">risk<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> this could happen.</p>
<p>Encrypted messages have this same risk of being modified, however, public key cryptography allows us a way to double-check if information has been tampered with, by double-checking someone’s digital identity with their real-life identity.</p>
<p>The public key is a giant block of text in a file. It is also represented in a human-readable shortcut called a<strong> </strong><a href="/en/glossary/key-fingerprint" class="glossify-link">key fingerprint<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>.</p>
<p><img alt="" src="/files/2018/11/14/keyconcepts9.png" width="433" height="433"></p>
<p>The word “<a href="/en/glossary/fingerprint" class="glossify-link">fingerprint<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>” means lots of different things in the field of computer security.</p>
<p>One use of the term is a “key fingerprint,” a string of characters like “65834 02604 86283 29728 37069 98932 73120 14774 81777 73663 16574 23234” that should allow you to uniquely and securely check that someone on the Internet is using the right private key.</p>
<p><img alt="" src="/files/2018/11/20/10.verifiation-signal.png" width="414" height="378"></p>
<p>In some apps, this information can be represented as a QR code that you and your friend scan off each other’s devices.</p>
<p>You can double-check that someone’s digital identity matches who they say they are through something called “fingerprint verification.”</p>
<p>Fingerprint verification is best done in real-life. If you’re able to meet with your friend in person, have your public key fingerprint available and let your friend double-check that every single character from your public key fingerprint matches what they have for your public key fingerprint. Checking a long string of characters like “342e 2309 bd20 0912 ff10 6c63 2192 1928” is tedious, but worth doing. If you’re not able to meet in person, you can make your fingerprint available through another secure channel, like another end-to-end encrypted messaging or chat system, or posted on a <a href="/en/glossary/https" class="glossify-link">HTTPS<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> site.</p>
<p class="warning">Verifying someone’s key fingerprint gives you a higher degree of certainty that it’s really them. But it’s not perfect because if the private keys are copied or stolen (say you have <a href="/en/glossary/malware" class="glossify-link">malware<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> on your device, or someone physically accessed your device and copied the file), someone else would be able to use the same fingerprint. For this reason, if a private key is “stolen,” you will want to generate a new public and private <a href="/en/glossary/key-pair" class="glossify-link">key pair<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>, and give your friends your new public key fingerprint.</p>
<a class="anchor-link-target" name="2">   </a> <h2>Summary: Public-Key <a href="/en/glossary/encryption" class="glossify-link">Encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> Capabilities <a class="anchor-link-icon" href="#2">Anchor link</a></h2>
<p>In general, using public-key encryption can provide users:</p>
<p><strong>Secrecy</strong>: A message encrypted with public-key <a href="/en/glossary/cryptography" class="glossify-link">cryptography<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> allows the sender to create a message that is secret, so that only the intended recipient can read it.</p>
<p><strong>Authenticity</strong>: A recipient of a message signed with public-key cryptography can verify that the message was authentically crafted by the sender if they have the sender’s public key.</p>
<p><strong>Integrity</strong>: A message signed or encrypted with public-key cryptography, in general, cannot be tampered with, otherwise the message will not <a href="/en/glossary/decrypt" class="glossify-link">decrypt<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> or verify correctly. This means that even unintentional disruption of a message (e.g. because of a temporary network problem) will be detectable.</p>
<h3>Identity Verification for Websites and Services: Security Certificates</h3>
<p>You might wonder: “I can verify public key fingerprints, but what’s the equivalent for the web? How can I double-check that I’m using a service that really is the service that it says it is? How can I be sure that no one is interfering with my connection to a service?”</p>
<p>Someone using <a href="/en/glossary/end-end-encryption" class="glossify-link">end-to-end encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> shares their public key widely so others can verify that they are who they say they are. Similarly, when using <a href="/en/glossary/transport-encryption" class="glossify-link">transport-layer encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>, your computer automatically checks to confirm whether a public key for a service is who it really says it is, and that it is encrypting to the intended service: this is called a <a href="/en/glossary/security-certificate" class="glossify-link">security certificate<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>.</p>
<p>Below, you can see an example of the security certificate for SSD from a generic <a href="/en/glossary/web-browser" class="glossify-link">Web browser<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>. This information is often accessible by clicking the HTTPS lock in your Web browser and pulling up the certificate details.</p>
<p><img alt="" src="/files/2018/11/20/11.httpsbrowser-ssd-cert.png" width="752" height="597"></p>
<p>The Web browser on your computer can make encrypted connections to sites using HTTPS. Websites often use security certificates to prove to your browser that you have a secure connection to the real site, and not to some other system that’s tampering with your connection. Web browsers examine certificates to check the public keys of domain names—(like <a href="http://www.google.com/" rel="noreferrer">www.google.com</a>, <a href="http://www.amazon.com/" rel="noreferrer">www.amazon.com</a>, or <a href="http://ssd.eff.org/">ssd.eff.org</a>). Certificates are one way of trying to determine if you know the correct public key for a person or website, so that you can communicate securely with them. But how does your computer know what the right public key is for sites you visit?</p>
<p>Modern browsers and operating systems include a list of trusted Certificate Authorities (CAs). The public keys for these CAs are pre-bundled when you download the browser or buy a computer. Certificate Authorities sign the public key of websites once they’ve validated them as legitimately operating a domain (such as <a href="http://www.example.com" rel="noreferrer">www.example.com</a>). When your browser visits an HTTPS site, it verifies that the certificate the site delivered has actually been signed by a CA that it trusts. This means that a trusted third-party has verified that the site is who they are claiming to be.</p>
<p class="warning">Just because a site’s security certificate has been signed by a Certificate Authority, does not mean that the website is necessarily a secure site. There are limits to what a CA can verify—it can’t verify that a website is honest or trustworthy. For example, a website may be “secured” using HTTPS, but still host scams and malware. Be vigilant, and learn more by <a href="https://ssd.eff.org/en/module/how-avoid-phishing-attacks">reading our guide on malware and phishing. </a></p>
<p class="warning">From time to time, you will see certificate-related error messages on the Web. Most commonly this is because a hotel or cafe network is trying to intercept your connection to a website in order to direct you to their login portal before accessing the web, or because of a bureaucratic mistake in the system of certificates. But occasionally it is because a hacker, thief, or police or spy agency is breaking the encrypted connection. Unfortunately, it is extremely difficult to tell the difference between these cases.</p>
<p>This means you should never click past a certificate warning if it relates to a site where you have an account or are reading any sensitive information.</p>
<h3>Putting It All Together: Symmetric Keys, Asymmetric Keys, &amp; Public Key Fingerprints.</h3>
<p><strong>The example of Transport-Layer Security Handshakes</strong></p>
<p>When using transport-layer encryption, your computer’s browser and the computer of the website you’re visiting are using both symmetric algorithms and asymmetric algorithms. </p>
<p>Let’s examine a concrete example of how all these ideas work together: when you connect to this HTTPS website (<a href="https://ssd.eff.org/">https://ssd.eff.org/</a>), what happens?</p>
<p>When a website uses HTTPS, your browser and the website’s server have a very fast set of interactions called “the handshake.” Your browser—the likes of Google Chrome, Mozilla Firefox, Tor Browser, and so forth—is talking to the server (computer) hosting our website, <a href="https://ssd.eff.org">https://ssd.eff.org</a>.</p>
<p>In the handshake, the browser and server first send each other notes to see if they have any shared preferences for encryption algorithms (these are known as “cipher suites”). You can think of it like your browser and our ssd.eff.org server are having a quick conversation: they’re asking each other what encryption methods they both know and should communicate in, as well as which encryption methods they prefer. (“Do we both know how to use an asymmetric algorithm like RSA in combination with a symmetric algorithm like AES? Yes, good. If this combination of encryption algorithms doesn’t work for us, what other encryption algorithms do we both know?”)</p>
<p>Then, your browser uses asymmetric encryption: it sends a public key certificate to ssd.eff.org to prove that you are who you say you are. The site's server checks this public key certificate against your public key. This is to prevent a malicious computer from intercepting your connection.</p>
<p>Once your identity is confirmed, the site’s server uses symmetric encryption: it generates a new, symmetric, secret key file. It then asymmetrically encrypts your browser’s public key, and sends it to your browser. Your browser uses its private key to decrypt this file.</p>
<p>If this symmetric key works, your browser and website’s server use it to <a href="/en/glossary/encrypt" class="glossify-link">encrypt<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> the rest of their communications. (This set of interactions is the <a href="/en/glossary/secure-sockets-layer-ssl" class="glossify-link">transport layer security<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> (TLS) handshake.) Thus, <a href="https://www.moserware.com/2009/06/first-few-milliseconds-of-https.html" rel="noreferrer">if all goes right</a> in the handshake, your connection to ssd.eff.org shows up as Secure, with HTTPS beside ssd.eff.org.</p>
<p>For a deeper dive on public and private keys, as well as verification, read our <a href="https://ssd.eff.org/en/node/50">SSD guide on public key encryption</a> next.</p>
</div>  </div>
</div> <!-- /.node -->
  
        </section>