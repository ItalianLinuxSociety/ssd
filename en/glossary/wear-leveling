<section class="main-section">
        <a id="main-content"></a>

        
                <div class="breadcrumb">
                  </div>
        
            <h1 class="page-header">Wear leveling</h1>
        
                                          
      
    <div id="taxonomy-term-347" class="taxonomy-term vocabulary-terms">

  
  <div class="content">
    <div class="field field-name-description-field field-type-text-with-summary field-label-hidden field-wrapper"><p>Some forms of digital storage, like the flash memory used in solid-state drives (SSD) and USB sticks, can wear out if overwritten many times. <a href="/en/glossary/wear-leveling" class="glossify-link">Wear leveling<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> is a method that spreads the writing of <a href="/en/glossary/data" class="glossify-link">data<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a> evenly across all of the media to prevent one part of it being overwritten too many times. Its benefit is that it can make devices last longer. The danger for security-conscious users is that wear leveling interferes with secure erase programs, which deliberately try to overwrite sensitive files with junk data in order to permanently erase them. Rather than trusting secure erase programs with files stored on SSD or USB flash drives, it can be better to use full-disk <a href="/en/glossary/encryption" class="glossify-link">encryption<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>. Encryption avoids the difficulty of secure erasing by making any file on the drive difficult to recover without the correct <a href="/en/glossary/passphrase" class="glossify-link">passphrase<img src="https://ssd.eff.org/sites/all/themes/ssd/img/info.png"></a>.</p>
</div><br>  </div>

</div>
  
        </section>