# Surveillance Self-Defense

In corso qui la traduzione in lingua italiana della guida Surveillance Self-Defense pubblicata da Electronic Frontier Foundation.

https://ssd.eff.org/

## Come procedere

 * per ogni file in en/module/ e en/glossary/ creare un analogo file (con il filename tradotto in italiano) nelle rispettive cartelle it/module/ e it/glossary/
 * mantenere per quanto possibile la formattazione HTML originaria e le relative classi CSS (ma è possibile ed anzi consigliato fare una reindentazione dell'HTML originario, affinché risulti più leggibile)
 * mantenere i paths originali delle immagini; saranno a posteriori scaricate e rilinkate
 * i links interni tra le pagine saranno revisionati quando ci saranno un po' di contenuti da effettivamente linkare tra loro

## Disclaimer

Trainers and teachers all around the world have independently localized and translated content from EFF’s Security Education Companion and Surveillance Self-Defense. Please note that EFF has not reviewed or checked this work, and cannot vouch for its accuracy or whether it is up to date. However, we are including these links in case it’s helpful to your teaching practice. In case of doubt, always compare them with the latest version on EFF's own websites, at https://ssd.eff.org/ and https://sec.eff.org/

